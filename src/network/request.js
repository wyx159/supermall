import axios from 'axios'


export function request(config) {
  //1.创建axios的shili
  const instance1 = axios.create({
    baseURL: 'http://152.136.185.210:8000/api/n3',
    //baseURL:'http://123.207.32.32:8000',
    timeout: 5000
  })

  //2.1请求拦截器
  instance1.interceptors.request.use(config => {
    console.log(config);
    //1.比如config中的一些信息不符合服务器的要求
    //2.比如每次发送请求时，都希望界面中显示一个请求的图标
    //3.某些网络请求（比如登陆（token））,必须携带一些特殊的信息
    return config;
  }, err => {
    console.log(err)
  })
  //2.2 响应拦截器
  instance1.interceptors.response.use(res => {
    //console.log(res);
    return res.data
  }, err => {
    console.log(err);
  })
  //发送真正的请求
  return instance1(config);
}
